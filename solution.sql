/*
MySQL Advanced Selects and Joining Tables (Activity)
Create an solution.sql file inside s04/a1 project and do the following using the music_db database:
1. Find all artists that has letter d in its name.
*/
SELECT * FROM artists WHERE name LIKE "%d%";

-- 2. Find all songs that has a length of less than 4 minutes.
SELECT * FROM songs WHERE length < 400;

-- 3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albums.album_title, songs.length FROM albums LEFT JOIN songs ON albums.id = songs.album_id;

-- 4. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT * FROM artists LEFT JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";
-- 5. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC limit 4;

-- 6. Join the 'albums' and 'songs' tables and find all songs greater than 3 minutes 30 seconds. (Sort albums from Z-A)
SELECT * FROM albums LEFT JOIN songs ON albums.id = songs.album_id WHERE songs.length > 330;
-- Submit your activity by 9:30 pm :WDC040-4 | MySQL Advanced Select with Joins